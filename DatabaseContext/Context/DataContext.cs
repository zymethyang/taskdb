﻿using Microsoft.EntityFrameworkCore;

namespace DatabaseContext.Context
{
    public static class DataContext
    {
        public static ApplicationDbContext CreateContext()
        {
            var optionBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionBuilder.UseSqlServer(Common.GlobalEnv.ConnectionString);
            return new ApplicationDbContext(optionBuilder.Options);
        }
    }
}
