﻿using Microsoft.EntityFrameworkCore;

namespace DatabaseContext.Context
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Common.GlobalEnv.ConnectionString);
            }
        }

        public virtual DbSet<EFModels.Task> Tasks { get; set; }
    }
}
