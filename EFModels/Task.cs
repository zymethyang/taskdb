﻿using System.ComponentModel.DataAnnotations;
#nullable disable

namespace EFModels
{
    public partial class Task
    {
        public int Id { get; set; }

        [Required]
        public string TaskId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Priority { get; set; } = Common.TaskPriority.MEDIUM;

        [Required]
        public DateTime DueDate { get; set; }

        public string Status { get; set; } = Common.TaskStatus.TODO;
    }
}
