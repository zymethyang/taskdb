﻿using DatabaseContext.Context;
using Microsoft.EntityFrameworkCore;

namespace TaskManagement.Server.Services
{
    public class TaskService
    {
        public TaskService(ApplicationDbContext context)
        {
            _context = context;
        }

        private ApplicationDbContext _context;

        public async Task<IEnumerable<EFModels.Task>> GetTasks(string? status, string? priority)
        {
            IEnumerable<EFModels.Task> tasks = await _context.Tasks.ToListAsync();
            IEnumerable<EFModels.Task> response = tasks;

            if (!string.IsNullOrEmpty(status))
            {
                response = tasks.Where(task => task.Status == status).ToList();
            }

            if (!string.IsNullOrEmpty(priority))
            {
                response = response.Where(task => task.Priority == priority).ToList();
            }

            return response;
        }

        public async Task<EFModels.Task> CreateTask(EFModels.Task task)
        {
            await _context.Tasks.AddAsync(task);
            await _context.SaveChangesAsync();
            return task;
        }

        public async Task<int> UpdateTask(int taskId, EFModels.Task task)
        {
            var foundTask = await _context.Tasks.Where(task => task.Id == taskId).FirstOrDefaultAsync();
            if (foundTask != default)
            {
                foundTask.DueDate = task.DueDate;
                foundTask.Status = task.Status;
                foundTask.Title = task.Title;
                foundTask.Description = task.Description;
                foundTask.Priority = task.Priority;
                _context.Tasks.Update(foundTask);
                await _context.SaveChangesAsync();
                return foundTask.Id;
            }

            return -1;
        }

        public async Task<int> DeleteTask(int taskId)
        {
            var foundTask = await _context.Tasks.Where(task => task.Id == taskId).FirstOrDefaultAsync();
            if (foundTask != default)
            {
                _context.Remove(foundTask);
                await _context.SaveChangesAsync();
                return foundTask.Id;
            }
            return -1;
        }
    }
}
