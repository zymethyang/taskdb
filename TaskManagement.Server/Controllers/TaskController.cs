﻿using DatabaseContext.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskManagement.Server.Services;

namespace TaskManagement.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {

        public TaskController(ApplicationDbContext context, TaskService taskService)
        {
            _context = context;
            _taskService = taskService;
        }

        private ApplicationDbContext _context;
        private TaskService _taskService;

        [HttpGet]
        [ProducesResponseType<IEnumerable<EFModels.Task>>(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromQuery] string? status, [FromQuery] string? priority)
        {
            IEnumerable<EFModels.Task> tasks = await _taskService.GetTasks(status, priority);
            return Ok(tasks);
        }

        [HttpPost]
        [ProducesResponseType<EFModels.Task>(StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody] EFModels.Task task)
        {
            EFModels.Task newTask = await _taskService.CreateTask(task);
            return Ok(newTask);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Put(int id, [FromBody] EFModels.Task task)
        {
            var result = await _taskService.UpdateTask(id, task);
            if (result > 0)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _taskService.DeleteTask(id);
            if (result > 0)
            {
                return Ok();
            }

            return NotFound();
        }
    }
}
