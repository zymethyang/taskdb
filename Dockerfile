FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env

WORKDIR /App

RUN curl -fsSL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh

RUN apt update -y 

RUN apt install -y nodejs npm

RUN npm install -g yarn

COPY . ./

RUN dotnet restore

RUN cd taskmanagement.client && yarn install && cd ../

RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:8.0

WORKDIR /App

COPY --from=build-env /App/out .

ENTRYPOINT ["dotnet", "TaskManagement.Server.dll"]