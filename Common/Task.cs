﻿namespace Common
{
    public static class TaskStatus
    {
        public static string TODO = "todo";
        public static string IN_PROGRESS = "in-progress";
        public static string COMPLETED = "completed";
    }

    public static class TaskPriority
    {
        public static string LOW = "low";
        public static string MEDIUM = "medium";
        public static string HIGH = "high";
    }
}
