﻿using DatabaseContext.Context;
using Microsoft.EntityFrameworkCore;
using TaskManagement.Server.Services;

namespace TaskManagement.ServerTests.Controllers
{
    [TestClass]
    public class TaskServiceTests
    {
        private EFModels.Task mockTask = new EFModels.Task
        {
            Title = "Test-Ec1KM5MPafX36LBcXKMC",
            Description = "Test",
            DueDate = DateTime.Now,
            Priority = Common.TaskPriority.LOW,
            Status = Common.TaskStatus.TODO,
            TaskId = "Ec1KM5MPafX36LBcXKMC",
        };

        [TestMethod]
        public async Task GetTasksTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);
            IEnumerable<EFModels.Task> tasks = await taskService.GetTasks(null, null);

            foreach (EFModels.Task task in tasks)
            {
                Assert.AreEqual(mockTask.Title, task.Title, "New Title should equal");
                Assert.AreEqual(mockTask.DueDate, task.DueDate, "New DueDate should equal");
                Assert.AreEqual(mockTask.Priority, task.Priority, "New Priority should equal");
                Assert.AreEqual(mockTask.Status, task.Status, "New Status should equal");
            }
        }

        [TestMethod]
        public async Task GetTasksWithStatusFilterTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);

            mockTask.Status = Common.TaskStatus.TODO;
            await taskService.CreateTask(mockTask);
            IEnumerable<EFModels.Task> tasks = await taskService.GetTasks("todo", null);

            foreach (EFModels.Task task in tasks)
            {
                Assert.AreEqual(task.Status, Common.TaskStatus.TODO, "Status should equal");
            }
        }

        [TestMethod]
        public async Task GetTasksWithPriorityFilterTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);

            mockTask.Priority = Common.TaskPriority.MEDIUM;
            await taskService.CreateTask(mockTask);
            IEnumerable<EFModels.Task> tasks = await taskService.GetTasks(null, "medium");

            foreach (EFModels.Task task in tasks)
            {
                Assert.AreEqual(task.Priority, Common.TaskPriority.MEDIUM, "New Priority should equal");
            }
        }

        [TestMethod]
        public async Task CreateTaskTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);
            EFModels.Task task = await taskService.CreateTask(mockTask);

            Assert.AreEqual(mockTask.Title, task.Title, "New Title should equal");
            Assert.AreEqual(mockTask.Description, task.Description, "New Description should equal");
            Assert.AreEqual(mockTask.DueDate, task.DueDate, "New DueDate should equal");
            Assert.AreEqual(mockTask.Priority, task.Priority, "New Priority should equal");
            Assert.AreEqual(mockTask.Status, task.Status, "New Status should equal");
        }

        [TestMethod]
        public async Task CreateNullTaskTest()
        {
            try
            {
                ApplicationDbContext context = DataContext.CreateContext();
                TaskService taskService = new TaskService(context);
                await taskService.CreateTask(new EFModels.Task());
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.AreEqual(ex.Message, "An error occurred while saving the entity changes. See the inner exception for details.", "Should throw error when create null");
            }
        }

        [TestMethod]
        public async Task UpdateTaskTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);

            IEnumerable<EFModels.Task> tasks = await context.Tasks.Where(t => t.Title == mockTask.Title).ToListAsync();

            foreach (EFModels.Task foundTask in tasks)
            {
                foundTask.Description = "test-update";
                await taskService.UpdateTask(foundTask.Id, foundTask);
            }

            IEnumerable<EFModels.Task> updatedTasks = await context.Tasks.Where(t => t.Title == mockTask.Title).ToListAsync();
            foreach (EFModels.Task foundTask in updatedTasks)
            {
                Assert.AreEqual(foundTask.Description, "test-update", "New Description should equal");
            }
        }

        [TestMethod]
        public async Task UpdateEmptyTaskTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);
            int response = await taskService.UpdateTask(-1, new EFModels.Task());
            Assert.AreEqual(response, -1, "Should return status code -1");
        }

        [TestMethod]
        public async Task UpdateNullTaskTest()
        {
            try
            {
                ApplicationDbContext context = DataContext.CreateContext();
                TaskService taskService = new TaskService(context);

                IEnumerable<EFModels.Task> tasks = await context.Tasks.Where(t => t.Title == mockTask.Title).ToListAsync();

                foreach (EFModels.Task foundTask in tasks)
                {
                    foundTask.Description = "test-update";
                    await taskService.UpdateTask(foundTask.Id, new EFModels.Task());
                    Assert.Fail();
                }
            }
            catch (Exception ex)
            {
                Assert.AreEqual(ex.Message, "An error occurred while saving the entity changes. See the inner exception for details.", "Should throw error when create null");
            }
        }

        [TestMethod]
        public async Task DeleteTaskTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);

            IEnumerable<EFModels.Task> tasks = await context.Tasks.Where(t => t.Title == mockTask.Title).ToListAsync();

            foreach (EFModels.Task foundTask in tasks)
            {
                await taskService.DeleteTask(foundTask.Id);
            }

            IEnumerable<EFModels.Task> remainTasks = await context.Tasks.Where(t => t.Title == mockTask.Title).ToListAsync();
            Assert.AreEqual(remainTasks.Count(), 0, "Should not have items");
        }

        [TestMethod]
        public async Task DeleteEmptyTaskTest()
        {
            ApplicationDbContext context = DataContext.CreateContext();
            TaskService taskService = new TaskService(context);
            int response = await taskService.DeleteTask(-1);
            Assert.AreEqual(response, -1, "Should return status code -1");
        }
    }
}
