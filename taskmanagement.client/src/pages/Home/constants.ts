import { ICommonHeader } from "../../components/Table";

export const headers: ICommonHeader[] = [
    {
        title: "Task Id",
        name: "taskId",
    },
    {
        title: "Title",
        name: "title",
    },
    {
        title: "Description",
        name: "description",
    },
    {
        title: "Priority",
        name: "priority",
    },
    {
        title: "Due Date",
        name: "dueDate",
    },
    {
        title: "Status",
        name: "status",
    },
    {
        title: "Edit",
        name: "edit",
    },
    {
        title: "Delete",
        name: "delete",
    }
];
