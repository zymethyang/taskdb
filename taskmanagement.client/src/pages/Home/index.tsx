import { Button, Container, Grid } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import { useEffect, useState } from "react";
import { v4 as uuidV4 } from "uuid";
import { toast } from "react-toastify";
import { FormProvider, useForm } from "react-hook-form";
import dayjs from "dayjs";
import {
    createTask as createTaskAPI,
    updateTask as updateTaskAPI,
    getTasks as getTasksAPI,
    deleteTask as deleteTaskAPI,
} from "../../api/task";
import CommonTable from "../../components/Table";
import TaskModal from "../../components/TaskModal";
import { headers } from "./constants";
import { ITask, PRIORITY_OPTIONS, STATUS_OPTIONS } from "../../interfaces/Task/task";
import { getTaskStatusLabel } from "./utils";
import { ETaskPriority, ETaskStatus } from "../../constants/task";
import CommonSelect from "../../components/Select";
import styles from "./home.module.scss";

const Home = () => {
    const methods = useForm<ITask>({
        mode: "onChange",
    });
    const [tasks, setTasks] = useState<ITask[]>([]);
    const [statusFilter, setStatusFilter] = useState<ETaskStatus>('' as ETaskStatus);
    const [priorityFilter, setPriorityFilter] = useState<ETaskPriority>('' as ETaskPriority);
    const [editTaskId, setEditTaskId] = useState<number>(0);
    const [open, setOpen] = useState<boolean>(false);

    async function fetchTasks(): Promise<void> {
        setTasks(await getTasksAPI(statusFilter, priorityFilter));
    }

    useEffect(() => {
        fetchTasks();
    }, [statusFilter, priorityFilter]);

    async function createTask(formValues: ITask): Promise<void> {
        try {
            const newTask: Partial<ITask> = {
                title: formValues.title,
                description: formValues.description,
                dueDate: dayjs(formValues.dueDate).toDate(),
                priority: formValues.priority,
                status: formValues.status,
                taskId: uuidV4(),
            };
            await createTaskAPI(newTask);
            await fetchTasks();
            toast.success("Task created successfully");
        } catch (error) {
            toast.error("Failed to create task");
        } finally {
            methods.reset({});
            setOpen(false);
        }
    }

    async function editTask(formValues: ITask): Promise<void> {
        try {
            const updateTask: Partial<ITask> = {
                title: formValues.title,
                description: formValues.description,
                dueDate: dayjs(formValues.dueDate).toDate(),
                priority: formValues.priority,
                status: formValues.status,
                taskId: formValues.taskId,
            };
            await updateTaskAPI(editTaskId, updateTask);
            await fetchTasks();
            toast.success("Task created successfully");
        } catch (error) {
            toast.error("Failed to create task");
        } finally {
            methods.reset({});
            setOpen(false);
            setEditTaskId(0);
        }
    }

    async function deleteTask(taskId: number): Promise<void> {
        try {
            await deleteTaskAPI(taskId);
            await fetchTasks();
            toast.success("Task deleted successfully");
        } catch (error) {
            toast.error("Failed to delete task");
        }
    }

    function cancelTask(): void {
        methods.reset({});
        setEditTaskId(0);
        setOpen(false);
        toast.info("Task creation cancelled");
    }

    return (
        <FormProvider {...methods}>
            <Container className={styles.container}>
                <Grid className={styles.table}>
                    <Grid className={styles.header}>
                        <Grid className={styles.select}>
                            <CommonSelect
                                value={statusFilter}
                                defaultValue={ETaskStatus.TODO}
                                options={STATUS_OPTIONS}
                                onChange={(value) => setStatusFilter(value as ETaskStatus)}
                                className={styles.dropdown}
                            />
                            <CommonSelect
                                value={priorityFilter}
                                defaultValue={ETaskPriority.MEDIUM}
                                options={PRIORITY_OPTIONS}
                                onChange={(value) => setPriorityFilter(value as ETaskPriority)}
                                className={styles.dropdown}
                            />
                        </Grid>
                        <Button 
                            className={styles.button} 
                            onClick={() => {
                                setOpen(true)
                                methods.reset({});
                            }} 
                            variant="contained">
                            Create
                        </Button>
                    </Grid>
                    <Grid className={styles.content}>
                        {tasks?.length > 0 && (
                            <CommonTable
                                rows={tasks.map((task) => ({
                                    ...task,
                                    dueDate: dayjs(task.dueDate).format("MM/DD/YYYY"),
                                    status: getTaskStatusLabel(task.status),
                                    edit: (
                                        <ModeEditIcon
                                            className={styles.editIcon}
                                            onClick={() => {
                                                methods.reset(task);
                                                setEditTaskId(task.id);
                                                setOpen(true);
                                            }}
                                        />
                                    ),
                                    delete: (
                                        <DeleteIcon
                                            className={styles.deleteIcon}
                                            onClick={() => deleteTask(task.id)}
                                        />
                                    ),
                                }))}
                                headers={headers}
                            />
                        )}
                        {!tasks?.length && <h3>No tasks found</h3>}
                    </Grid>
                </Grid>
                <TaskModal open={open} cancel={cancelTask} onSubmit={editTaskId ? editTask : createTask} />
            </Container>
        </FormProvider>
    );
};

export default Home;
