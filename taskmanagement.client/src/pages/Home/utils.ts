import { ETaskStatus } from "../../constants/task";

export function getTaskStatusLabel(status: ETaskStatus): string {
    switch (status) {
        case ETaskStatus.TODO:
            return "To Do";
        case ETaskStatus.IN_INPROGRESS:
            return "In Progress";
        case ETaskStatus.DONE:
            return "Done";
        default:
            return "";
    }
}