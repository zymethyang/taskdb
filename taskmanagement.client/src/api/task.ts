import { ETaskPriority, ETaskStatus } from '../constants/task';
import { ITask } from '../interfaces/Task/task';
import { api } from './index';

export async function createTask(task: Partial<ITask>): Promise<ITask> {
    const response = await api.post<ITask>('/task', task);
    return response.data;
}

export async function updateTask(taskId: number, task: Partial<ITask>): Promise<void> {
    await api.put<ITask>(`/task/${taskId}`, task);
}

export async function getTasks(status?: ETaskStatus, priorityFilter?: ETaskPriority): Promise<ITask[]> {
    const response = await api.get<ITask[]>(`/task?status=${status}&priority=${priorityFilter}`);
    return response.data;
}

export async function deleteTask(taskId: number): Promise<void> {
    await api.delete(`/task/${taskId}`);
}