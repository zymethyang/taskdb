export enum ETaskStatus {
    TODO = "todo",
    IN_INPROGRESS = "in-progress",
    DONE = "done"
}

export enum ETaskPriority {
    LOW = "low",
    MEDIUM = "medium",
    HIGH = "high"
}