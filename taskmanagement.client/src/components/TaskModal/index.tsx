import { TextField, Grid, Button } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import {
    Controller,
    SubmitHandler,
    useFormContext,
} from "react-hook-form";
import CommonSelect from "../Select";
import CommonModal from "../Modal";
import { ITask, PRIORITY_OPTIONS, STATUS_OPTIONS } from "../../interfaces/Task/task";
import styles from "./taskModal.module.scss";
import dayjs from "dayjs";
import { ETaskPriority, ETaskStatus } from "../../constants/task";

interface ITaskModalProps {
    open: boolean;
    cancel: () => void;
    onSubmit: SubmitHandler<ITask>;
}

const TaskModal = (props: ITaskModalProps) => {
    const { open, cancel, onSubmit } = props;
    const { register, handleSubmit, reset, control, formState } =
        useFormContext<ITask>();

    function resetForm(): void {
        reset({});
        cancel();
    }

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <CommonModal open={open}>
                <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
                    <TextField
                        {...register("title", {
                            required: { value: true, message: "Title is required" },
                        })}
                        className={styles.textField}
                        label={formState.errors.title?.message ?? "Title"}
                        color={formState.errors.title?.message ? "error" : "info"}
                        variant="standard"
                    />
                    <TextField
                        {...register("description", {
                            required: { value: true, message: "Description is required" },
                        })}
                        className={styles.textField}
                        label={formState.errors.description?.message ?? "Description"}
                        color={formState.errors.description?.message ? "error" : "info"}
                        variant="standard"
                    />
                    <Controller
                        name="priority"
                        control={control}
                        render={({ field }) => (
                            <CommonSelect
                                className={styles.textField}
                                defaultValue={ETaskPriority.MEDIUM}
                                value={field.value}
                                label="Priority"
                                options={PRIORITY_OPTIONS.filter(task => !!task.value)}
                                onChange={field.onChange}
                            />
                        )}
                    />
                    <Controller
                        name="dueDate"
                        control={control}
                        render={({ field }) => (
                            <DatePicker
                                defaultValue={dayjs()}
                                value={dayjs(field.value)}
                                className={styles.datePicker}
                                label="Due Date"
                                onChange={field.onChange}
                            />
                        )}
                    />
                    <Controller
                        name="status"
                        control={control}
                        render={({ field }) => (
                            <CommonSelect
                                className={styles.textField}
                                defaultValue={ETaskStatus.TODO}
                                value={field.value}
                                label="Status"
                                options={STATUS_OPTIONS.filter(task => !!task.value)}
                                onChange={field.onChange}
                            />
                        )}
                    />
                    <Grid className={styles.footer}>
                        <Button
                            className={styles.cancel}
                            variant="outlined"
                            color="error"
                            onClick={resetForm}
                        >
                            Cancel
                        </Button>
                        <Button
                            className={styles.submit}
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            Save
                        </Button>
                    </Grid>
                </form>
            </CommonModal>
        </LocalizationProvider>
    );
};

export default TaskModal;
