import { Modal, Box } from '@mui/material'
import styles from './modal.module.scss'

interface ICommonModalProps {
    children: React.ReactNode
    open: boolean
}

const CommonModal = (props: ICommonModalProps) => {
    const { open, children } = props
    return (
        <Modal className={styles.container} open={open}>
            <Box className={styles.box}>
                {children}
            </Box>
        </Modal>
    )
}

export default CommonModal;