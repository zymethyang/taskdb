import { MenuItem, Select, Typography, Grid } from "@mui/material";
import styles from './select.module.scss';

export interface IOption {
    label: string;
    value: string;
}

export interface ISelectProps {
    value: string;
    options: IOption[];
    onChange: (value: string) => void;
    label?: string;
    className?: string;
    defaultValue?: string;
}

const CommonSelect = (props: ISelectProps) => {
    const { value, label, onChange, options, className, defaultValue} = props;
    return (
        <Grid className={className}>
            {label && <Typography>{label}</Typography>}
            <Select className={styles.select} defaultValue={defaultValue} value={value} onChange={(event) => onChange(event.target.value)}>
                {options?.map((option: IOption) => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </Select>
        </Grid>
    );
};

export default CommonSelect;
