import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export interface ICommonHeader {
    title: string
    name: string
}

interface ICommonTableProps {
    headers: ICommonHeader[]
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    rows: any[]
}

const CommonTable = (props: ICommonTableProps) => {
    const { headers, rows } = props;

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        {
                            headers?.map((header: ICommonHeader) => (
                                <TableCell key={header.name}>{header.title}</TableCell>)
                            )
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row?.id}>
                            {
                                headers?.map((header: ICommonHeader) => (
                                    <TableCell key={`${row?.id}-${header.name}`} component="th" scope="row">
                                        {row?.[header.name]}
                                    </TableCell>)
                                )
                            }
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}

export default CommonTable;