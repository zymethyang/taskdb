import { ToastContainer } from 'react-toastify'
import Home from "./pages/Home";
import 'react-toastify/dist/ReactToastify.css';
import "./App.css";

const App = () => {
    return <>
        <Home />
        <ToastContainer theme='colored' />
    </>;
};

export default App;
