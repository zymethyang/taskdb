import { IOption } from "../../components/Select";
import { ETaskPriority, ETaskStatus } from "../../constants/task";

export interface ITask {
    id: number;
    taskId: string;
    title: string;
    description: string;
    priority: string;
    dueDate: Date;
    status: ETaskStatus;
}

export const STATUS_OPTIONS : IOption[]= [
    {
        label: 'ALL',
        value: '' as ETaskStatus
    },
    {
        label: 'TO DO',
        value: ETaskStatus.TODO
    },
    {
        label: 'IN PROGRESS',
        value:ETaskStatus.IN_INPROGRESS
    },
    {
        label: 'DONE',
        value: ETaskStatus.DONE
    }
]

export const PRIORITY_OPTIONS : IOption[]= [
    {
        label: 'ALL',
        value: '' as ETaskStatus
    },
    {
        label: 'Low',
        value: ETaskPriority.LOW
    },
    {
        label: 'Medium',
        value: ETaskPriority.MEDIUM
    },
    {
        label: 'High',
        value: ETaskPriority.HIGH
    }
]