# HOW TO START TEST MODE

1. Setup docker https://docs.docker.com/engine/install/ubuntu/

2. Run `docker compose up -d --build --force-recreate --remove-orphans`

3. Access: http://localhost:8080

# HOW TO START DEV MODE

1. Setup SQL Server 2022.

2. Update ConnectionString in GlobalEnv.

3. Setup Visual Studio & .NET core

4. Start project TaskManagement.Server

5. Access: https://localhost:5173/

# ADDITIONAL RESOURCES

1. SQL query to create Task table.
```sql
CREATE TABLE Task (
  Id int PRIMARY KEY IDENTITY,
  TaskId nvarchar(255) NOT NULL,
  Title nvarchar(255) NOT NULL,
  Description nvarchar(max) NOT NULL,
  Priority nvarchar(50) DEFAULT 'MEDIUM',
  DueDate datetime NOT NULL,
  Status nvarchar(50) DEFAULT 'TODO'
);
```
Entity Framework Core supports create table automatically. The query above equivalent to project DatabaseContext.